# READ "_README.txt" FIRST BEFORE PLOTTING!
Real WorldMap
1 1 S 0 S S S 0 0 S 1 1
S S 0 8 0 0 0 0 0 0 B 0
0 0 B 0 0 0 0 0 0 0 0 1
0 0 0 1 0 0 0 0 1 0 B V
0 0 1 0 0 0 0 0 0 0 0 0
7 0 0 0 0 0 R R 0 0 0 1
1 1 T T T T 1 1 T T 1 1
1 1 1 1 1 1 1 1 1 1 1 1
#
Imag WorldMap
1 1 S 0 S S S 0 0 S 1 1
S S 0 0 0 0 0 0 0 0 B 0
0 0 B 0 0 0 0 0 0 0 8 1
0 0 0 1 0 0 0 0 1 0 B V
0 0 0 0 0 0 0 0 0 8 0 0
7 0 0 0 0 0 R R 0 0 0 1
1 1 T T T T 1 1 T T T T
1 1 1 1 1 1 1 1 1 1 1 1
End