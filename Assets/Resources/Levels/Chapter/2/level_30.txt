# READ "_README.txt" FIRST BEFORE PLOTTING!
Real WorldMap
0 0 0 0 1 0 0 0 0 0 S 0
7 8 0 0 V 0 0 0 0 0 0 0
0 0 0 0 0 0 R R 0 0 0 0
R R R 0 0 0 0 0 0 T T T
T T T T T T T 0 0 1 1 1
1 1 1 1 1 1 1 T 0 0 0 0
2 2 2 2 2 2 2 1 1 1 2 2
2 2 2 2 2 2 2 1 1 1 1 1
#
Imag WorldMap
0 0 0 0 1 0 0 0 0 0 8 0
0 0 0 0 V 0 0 0 0 0 0 0
0 0 0 0 0 0 R R 0 0 0 0
R R R 0 0 0 0 0 0 T T T
T T T T T T T 0 0 1 1 1
1 1 1 1 1 1 1 T 0 0 0 0
2 2 2 2 2 2 2 1 1 1 3 2
2 2 2 2 2 2 2 1 1 1 1 1
End