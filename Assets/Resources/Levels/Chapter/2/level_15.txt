# READ "_README.txt" FIRST BEFORE PLOTTING!
Real WorldMap
0 0 0 0 0 1 1 0 0 0 0 8
0 0 T T 0 0 V S T T T 0
0 0 1 1 0 0 0 0 1 S 1 0
7 0 1 1 0 0 0 0 0 0 V 0
1 B V S 0 0 0 0 0 0 0 0
1 0 A 0 0 0 0 0 0 0 0 0
1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1
#
Imag WorldMap
0 0 0 0 0 1 1 0 0 0 0 0
0 0 T T 0 8 V S T T T 0
0 0 1 1 0 0 0 8 1 S 1 0
7 0 1 1 0 0 0 0 0 0 V 0
1 B V S 0 0 0 0 0 0 0 0
1 0 A 0 0 0 0 0 0 0 0 0
1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1
End