# READ "_README.txt" FIRST BEFORE PLOTTING!
Real WorldMap
0 0 0 0 0 8 0 0 S S S 0
0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 R R R 0
0 0 0 1 0 0 T 0 0 0 0 0
0 0 0 1 0 8 1 0 0 0 1 1
0 0 0 1 0 0 A 0 0 0 1 1
7 A 0 1 1 1 1 1 T T 1 1
1 1 1 1 T T 1 1 1 1 1 1
#
Imag WorldMap
0 0 0 0 0 0 0 0 S S S 0
0 0 0 0 0 0 0 0 0 0 0 0
8 0 0 0 0 0 0 0 R R R 0
0 0 0 1 0 0 T 0 0 0 0 0
0 0 0 1 0 0 1 0 0 0 1 1
0 0 0 1 0 0 A 0 0 0 1 1
7 A 0 1 0 0 1 1 T T 1 1
1 1 1 1 T T 1 1 1 1 1 1
End