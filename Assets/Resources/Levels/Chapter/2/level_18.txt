# READ "_README.txt" FIRST BEFORE PLOTTING!
Real WorldMap
0 0 0 V S 8 0 S V 0 0 0
0 0 B 0 0 0 0 0 0 B 0 0
B 1 R R T B B T R R 1 B
0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0
A 0 0 0 0 7 0 0 0 0 0 A
1 T T T R R R R T T T 1
1 1 1 1 1 1 1 1 1 1 1 1
#
Imag WorldMap
0 0 0 V S 0 8 S V 0 0 0
0 0 B 0 0 0 0 0 0 B 0 0
B 1 R R T B B T R R 1 B
0 0 0 0 0 8 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0
A 0 0 0 0 7 0 0 0 0 0 A
1 T T T R R R R T T T 1
1 1 1 1 1 1 1 1 1 1 1 1
End