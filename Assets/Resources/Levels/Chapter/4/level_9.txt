# READ "_README.txt" FIRST BEFORE PLOTTING!
Real WorldMap
0 0 0 0 0 0 1 0 0 0 0 1
7 0 0 0 8 1 1 0 0 0 0 G
1 0 0 1 1 1 0 0 0 0 0 0
0 0 W G 0 Y 0 0 W 0 0 0
1 0 W 0 0 0 0 W 0 0 0 1
0 1 W 0 1 1 W 0 0 0 0 Y
0 0 W 0 0 0 0 0 0 0 1 1
0 0 0 0 0 0 0 0 0 1 0 0
#
Imag WorldMap
0 0 0 0 0 0 1 0 0 0 0 1
0 0 0 0 0 1 1 0 0 0 0 G
1 0 0 1 1 1 0 0 0 0 0 0
8 0 W G 0 Y 0 0 W 0 0 0
1 0 W 0 0 0 0 W 0 0 0 1
0 0 W 0 1 1 W 0 0 0 0 Y
0 0 W 0 0 0 0 0 0 8 1 1
0 0 0 0 0 0 0 0 0 0 0 0
End