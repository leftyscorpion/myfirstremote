# READ "_README.txt" FIRST BEFORE PLOTTING!
Real WorldMap
1 0 0 0 0 0 1 0 0 0 0 0
G 0 0 0 0 0 1 0 0 0 0 0
0 0 0 8 0 0 G 0 0 0 0 0
1 0 0 W 0 0 0 0 W W 0 0
Y 0 0 X 0 0 7 0 X Y 0 W
1 1 W 0 0 0 1 0 0 0 0 0
0 0 0 0 0 0 1 0 0 0 0 0
1 1 1 1 0 0 1 1 1 1 0 1
#
Imag WorldMap
1 0 0 0 0 0 1 0 0 0 0 0
G 0 0 0 0 0 1 0 0 0 0 0
0 0 0 0 0 0 G 0 0 0 0 0
1 0 0 W 0 0 0 0 W W 0 8
Y 0 0 X 0 0 7 0 X Y 0 W
1 1 W 0 0 0 1 0 0 0 0 0
8 0 0 0 0 0 1 0 0 0 0 0
1 1 1 1 0 0 1 1 1 1 0 1
End