# READ "_README.txt" FIRST BEFORE PLOTTING!
Real WorldMap
0 0 0 0 1 1 1 1 0 0 0 0
1 1 0 0 G 1 1 Y 0 0 1 1
7 0 0 0 0 0 8 0 0 0 0 8
1 1 W W 0 0 0 0 W W 1 1
0 0 0 0 1 0 0 1 0 0 G 0
0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 W 0 0 0
0 0 0 0 0 0 0 0 X X 1 1
#
Imag WorldMap
0 0 0 0 1 1 1 1 0 0 0 0
1 1 0 0 G 1 1 Y 0 0 1 1
7 0 0 0 0 0 0 0 0 0 0 0
1 1 W W 0 0 0 0 W W 1 1
0 0 0 0 1 1 1 1 0 0 G 0
0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 W 0 0 8
0 0 0 0 0 0 0 0 X X 1 1
End