# READ "_README.txt" FIRST BEFORE PLOTTING!
Real WorldMap
0 0 0 0 0 0 0 0 S 0 S 0
1 0 1 1 1 1 1 0 0 0 0 0
1 8 1 V 0 S 1 0 0 1 0 0
1 V 1 1 1 1 1 1 0 0 0 0
0 0 0 0 0 1 1 V 0 0 0 0
0 0 0 0 0 1 1 0 0 0 0 0
7 0 0 0 0 1 1 0 0 0 T 0
1 1 1 1 1 1 1 T T T 1 T
#
Imag WorldMap
0 0 8 0 0 1 0 0 S 0 S 0
1 0 1 1 1 1 0 0 0 0 0 0
1 0 1 V 0 S 0 0 0 0 0 8
1 V 1 1 1 0 1 1 0 0 0 0
0 0 0 0 0 0 1 V 0 0 0 1
0 1 0 0 0 0 1 0 0 0 0 0
7 0 0 0 0 1 1 0 0 0 T 0
1 1 1 1 1 1 1 T T T 1 T
End