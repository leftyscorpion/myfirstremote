Until the time permits to have a GUI level editor, let's use this for a while.

Character notes for map matrices:
(Map Legend from Imaginarius_v2)
0  -> Space/Empty
1  -> Ground 
2  -> Water
3  -> Puzzle Pieces in Water
5  -> Ground (Left Corner)
6  -> Ground (Right Corner)
7* -> Player
8  -> Puzzle Pieces
9  -> Player in Water

(New Map Legend from Imaginarius_v3)
Chapter 1:
A  -> Bouldershroom
B  -> Bugs
Chapter 2:
S  -> Falling Stalagmite (the one that will fall via Bats)
V  -> Bat
T  -> Floor Stalagmite
Chapter 3:
R  -> Sliding Rock
L  -> Log in Water

[[NOTE: Still on prototyping stage]]
Chapter 4:
Y  -> Lights
P  -> Spider
Chapter 5:
M  -> Metal Beam
E  -> Electric Line


* Only for initial setup. Current coordinates won't be reverted to normal when they move
** Not yet implemented

NOTES:

1. The engine REQUIRES you to follow the prescribed row and column lengths (8 and 12, respectively) on plotting such level. Try to copy-paste the "sample.txt", change its name (see #2 for this one), then modify the map matrix there.

2. The filename for the levels must follow this format: level_#.txt (for example, level_12.txt)

3. It is recommended to add comments in the level file by adding "#" on the first line. Take note, however, that if a part of the map matrix has "#" on the beginning, the engine will get errors.

For more questions, please send an email to rocalworks@gmail.com.

- TRw-Ryxie