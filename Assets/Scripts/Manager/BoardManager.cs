using UnityEngine;
using System;
using System.Collections.Generic;

public class BoardManager : MonoBehaviour {
	
	public static BoardManager instance = null; // Singleton purposes
	public static List<Vector3> platformLocs = new List<Vector3>();
	public static List<Vector3> waterLocs = new List<Vector3>();
	public static List<Vector3> puzzleLocs = new List<Vector3>();
	public static List<Vector3> boulderLocs = new List<Vector3>();
	public static List<Vector3> beeLocs = new List<Vector3>();
	public static List<Vector3> logLocs = new List<Vector3>();
	public static List<Vector3> stalagLocs = new List<Vector3>();
	public static List<Vector3> upStalagLocs = new List<Vector3>();
	public static List<Vector3> batLocs = new List<Vector3>();
	public static List<Vector3> slipRockLocs = new List<Vector3>();
	public static List<Vector3> unstableRocksLocs = new List<Vector3>();
	public static List<Vector3> lightLocs = new List<Vector3>();
	public static List<Vector3> spiderLocs = new List<Vector3>();
	public static List<Vector3> unstableWoodLocs = new List<Vector3>();
	public static List<Vector3> beamLocs = new List<Vector3>();
	public static List<Vector3> elecLineLocs = new List<Vector3>();
	public static List<Vector3> springLocs = new List<Vector3>();
	public static List<Vector3> webLocs = new List<Vector3>();
	
	// Puzzle board size
	public int col = 8;
	public int row = 8;
	
	// List of tiles used in the game
	public GameObject[] platformTiles;
	public GameObject[] leftPlatformTiles;
	public GameObject[] rightPlatformTiles;
	public GameObject[] wallTiles;
	public GameObject[] puzzleTiles;
	public GameObject[] waterTiles;
	public GameObject[] boulshroomTiles;
	public GameObject[] beeTiles;
	public GameObject[] logTiles;
	public GameObject[] stalagTiles;
	public GameObject[] upStalagTiles;
	public GameObject[] batTiles;
	public GameObject[] slipRockTiles;
	public GameObject[] unstableRockTiles;
	public GameObject[] lightTiles;
	public GameObject[] spiderTiles;
	public GameObject[] unstableWoodTiles;
	public GameObject[] beamTiles;
	public GameObject[] elecLineTiles;
	public GameObject[] springTiles;
	public GameObject[] webTiles;
	// End of tile list
	
	// Movable objects already deployed
	public bool movableIn;
		
	// Set the walls on the edges of the board
	void BoardSetup() {
		Transform wallHolder; wallHolder = GameObject.Find("Wall").transform;	// Holder object for wall tiles
		
		for (int x = -1; x < col+1; x++) {
			for (int y = -1; y < row+1; y++) {
				// Left: x = -1 | Right: x= col | Bottom: y = -1 | Top: y = row
				if (x == -1 || x == col) {
					// Changes wall tiles according to current dimension
					GameObject tiles;
					if (GameManager.realworld) tiles = wallTiles[0];
					else tiles = wallTiles[1];
					// Create wall tiles and add them to the game via wallHolder
					GameObject instance = Instantiate(tiles, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
					instance.transform.SetParent(wallHolder);
				}
			}
		}
	}
	
	// Places the tiles in the game according to the map matrix (map)
	// NOTE: * Map matrices MUST have the same range as the columns and rows of the game
	//         e.g. if row = 5 & col = 5, then map matrix should be int[5,5].
	//       * C# doesn't accept variables for the length of their matrices, but there might be some ways for that.
	void LayoutObjects(GameObject[] gameTiles, char[,] map) {
		// NOTE(prolly bug): For some reasons, if we'll follow the conventional plotting [x = x; y = y], 
		//					 the map will inverted (probably because of the difference of coordinate planes in Unity
		//					 and in the map matrix). To get the right coordinated w.r.t. the map matrix, use this:
		//					 	for x-axis ->  x = y
		//						for y-axis ->  y = row-x-1
		for (int x = 0; x < row; x++) {
		
			for (int y = 0; y < col; y++) {
				// Get current coordinates
				Vector3 currPos = new Vector3(y, row-x-1, 0);
				
				// SPAWN PLATFORM TILE 
				if (map[x, y] == '1' || map[x, y] == '5' || map[x, y] == '6') {
					// Spawn tile
					GameObject instance = PlaceStaticTiles(gameTiles[0], platformLocs, currPos);
					
					if(x > 0) {
						if(map[x-1, y] != '1') {
							//change tile sprite
							instance.GetComponent<TileRandomizer>().TileAsPlatform();
						} else {
							instance.GetComponent<TileRandomizer>().TileAsTop();
						}
					} else {
						instance.GetComponent<TileRandomizer>().TileAsTop();
					}
				}
				// SPAWN WATER TILE
				else if (map[x, y] == '2') {
					GameObject instance = PlaceStaticTiles(gameTiles[1], waterLocs, currPos);
					if(x > 0) {
						if(map[x-1, y] == '2') {
							// disable animation
							instance.GetComponent<Animator>().enabled = false;
							// change tile sprite
							instance.GetComponent<TileRandomizer>().TileAsPlatform();
						}
					}
				} 

				// SPAWN PLAYER LOCATION
				else if (map[x, y] == '7') {
					// Change player's location according to map matrix
					GameManager.playerLoc = currPos;
					
				}	
				// SPAWN PUZZLE PIECES
				else if (map[x, y] == '8') {
					PlaceStaticTiles(gameTiles[2], puzzleLocs, currPos);
				}
				// SPAWN PLAYER IN THE WATER
				else if (map[x, y] == '9') {
					GameManager.playerLoc = currPos;
					GameObject temp = PlaceStaticTiles(gameTiles[1], waterLocs, currPos);
					if(x > 0) {
						if(map[x-1, y] == '2') {
							// disable animation
							temp.GetComponent<Animator>().enabled = false;
							// change tile sprite
							temp.GetComponent<TileRandomizer>().TileAsPlatform();
						}
					}
					
					// Shift to "water tile only" when player moves out
					if (GameManager.realworld) LevelLoader.rwlevel[x, y] = '2';
					else LevelLoader.iwlevel[x, y] = '2';
				}
				// SPAWN PUZZLE PIECE IN THE WATER
				else if (map[x, y] == '3') {
					// Add water tiles
					GameObject temp = PlaceStaticTiles(gameTiles[1], waterLocs, currPos);
					if(x > 0) {
						if(map[x-1, y] == '2') {
							// disable animation
							temp.GetComponent<Animator>().enabled = false;
							// change tile sprite
							temp.GetComponent<TileRandomizer>().TileAsPlatform();
						}
					}
					// Create puzzle tiles and add them to the game via levelHolder
					PlaceStaticTiles(gameTiles[2], puzzleLocs, currPos);
				}
				// SPAWN BOULDERSHROOM
				else if (map[x, y] == 'A') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[5], boulderLocs, currPos);
					}
				}
				// SPAWN BEES
				else if (map[x, y] == 'B') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[6], beeLocs, currPos);
					}
				}
				// SPAWN LOGS
				else if (map[x, y] == 'L') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[7], logLocs, currPos);
					}
				}
				// SPAWN BATS
				// TODO: Check if upper stalagmite is available
				else if (map[x, y] == 'V') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[8], batLocs, currPos);
					}
				}
				
				// SPAWN FALLING STALAGMITES
				else if (map[x, y] == 'S') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[9], stalagLocs, currPos);
					} 
				}
				
				// SPAWN STALAGMITES
				else if (map[x, y] == 'T') {
					PlaceStaticTiles(gameTiles[10], upStalagLocs, currPos);
				}
				
				// SPAWN SLIDING ROCKS
				else if (map[x, y] == 'R') {
					PlaceStaticTiles(gameTiles[11], slipRockLocs, currPos);
				}
				
				// SPAWN UNSTABLE ROCKS
				else if (map[x, y] == 'U') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[12], unstableRocksLocs, currPos);
					} 
				}
				
				// SPAWN LIGHTS
				else if (map[x, y] == 'Y' || map[x, y] == 'G') {
					if (!movableIn) {
						GameObject lit = PlaceMovableTiles(gameTiles[13], lightLocs, currPos);
						if (map[x, y] == 'G') 
							lit.transform.FindChild("Light").GetComponent<LightBulb>().isLighted = true;
					} 
				}
				
				// SPAWN SPIDER
				else if (map[x, y] == 'P') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[14], spiderLocs, currPos);
					} 
				}
				
				// SPAWN UNSTABLE WOOD
				else if (map[x, y] == 'W') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[15], unstableWoodLocs, currPos);
					}
				}
				
				// SPAWN METAL BEAM
				else if (map[x, y] == 'M') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[16], beamLocs, currPos);
					} 
				}
				
				// SPAWN ELECTRIC LINE
				else if (map[x, y] == 'E') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[17], elecLineLocs, currPos);
					} 
				}
				
				// SPAWN ELECTRIC LINE (HORIZONTAL)
				else if (map[x, y] == 'F') {
					if (!movableIn) {
						GameObject line = PlaceMovableTiles(gameTiles[17], elecLineLocs, currPos);
						line.transform.FindChild("Electricity").GetComponent<ElectricLine>().horizontal = true;
					} 
				}
				
				// SPAWN SPRING
				else if (map[x, y] == 'I') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[18], springLocs, currPos);
					} 
				}
				
				// SPAWN WEB
				else if (map[x, y] == 'X') {
					if (!movableIn) {
						PlaceMovableTiles(gameTiles[19], webLocs, currPos);
					} 
				}
			}
		}
		
		// Movable already deployed
		movableIn = true;
	}
	
	// Static = can't be moved; will be destroyed on dimension shift
	GameObject PlaceStaticTiles(GameObject tile, List<Vector3> poslist, Vector3 position) {
		return PlaceTile(GameObject.Find("StaticObj").transform, tile,  poslist, position);
	}
	
	// Movable = can be manipulated by the player; original position must retain
	GameObject PlaceMovableTiles(GameObject tile, List<Vector3> poslist, Vector3 position) {
		return PlaceTile(GameObject.Find("MovableObj").transform, tile, poslist, position);
	}
	
	// Place the tile to the board and store their positions
	GameObject PlaceTile(Transform type, GameObject tile, List<Vector3> poslist, Vector3 position) {
		// Create water tiles and add them to the game via levelHolder
		GameObject instance = Instantiate(tile, position, Quaternion.identity) as GameObject;
		instance.transform.SetParent(type);
		// Add coordinates for list (for water events)
		poslist.Add(position);
		return instance;
	}
	
	// Removes static tilesets 
	void RemoveStaticTiles() {
		// Cleaning everything on/before shifting the scene
		foreach (Transform children in GameObject.Find("StaticObj").transform) {
			Destroy(children.gameObject);
		}
		foreach (Transform children in GameObject.Find("Wall").transform) {
			Destroy(children.gameObject);
		}
	}
	
	// Removes all current tileset in the game
	void RemoveAllTiles() {
		// Cleaning everything on/before shifting the scene
		foreach (Transform children in GameObject.Find("Level").transform) {
			Destroy(children.gameObject);
		}
		foreach (Transform children in GameObject.Find("Wall").transform) {
			Destroy(children.gameObject); 
		}
		ClearLocations();
	}
	
	public static void ClearLocations() {
		// Clearing all the locations
		waterLocs.Clear();
		boulderLocs.Clear();
		platformLocs.Clear();
		waterLocs.Clear();
		puzzleLocs.Clear();
		boulderLocs.Clear();
		beeLocs.Clear();
		logLocs.Clear();
		stalagLocs.Clear();
		upStalagLocs.Clear();
		batLocs.Clear();
		slipRockLocs.Clear();
		unstableRocksLocs.Clear();
		lightLocs.Clear();
		spiderLocs.Clear();
		unstableWoodLocs.Clear();
		beamLocs.Clear();
		elecLineLocs.Clear();
		springLocs.Clear();
		webLocs.Clear();
	}
	
	// Sets up the board
	// TODO: Need to implement something for water and enemies
	public void SetupScene(char[,] layout) {
		// To avoid sudden removal of the tiles, check if the game loads for the first time.
		// If it doesn't, the game will clean the board for the next dimension
		if (!GameManager.firstTime) {
			RemoveStaticTiles();
		} else if (!GameManager.isPlayable) {
			RemoveAllTiles();
		}
		
		// Adds the walls
		BoardSetup();
		
		// Changes the platform depending on the player's current dimension
		// pT[0] = real world tileset; pT[1] = imaginary world tileset
		GameObject platform;
		if (GameManager.realworld) platform = platformTiles[0];
		else platform = platformTiles[1];
		
		// Change the corner platforms
		// Left corner
		GameObject platform_left;
		if (GameManager.realworld) platform_left = platformTiles[0];
		else platform_left = leftPlatformTiles[1];
		
		// Right corner
		GameObject platform_right;
		if (GameManager.realworld) platform_right = platformTiles[0];
		else platform_right = rightPlatformTiles[1];
		
		// Change the puzzle piece (see above)
		GameObject puzzle;
		if (GameManager.realworld) puzzle = puzzleTiles[0];
		else puzzle = puzzleTiles[1];
		
		// Boulder or mushroom? (see above)
		GameObject bs = boulshroomTiles[0];
		
		// Bees!!!
		GameObject bee = beeTiles[0];
		
		// Log o.O
		GameObject log = logTiles[0];
		
		// Bats
		GameObject bat = batTiles[0];
		
		// Stalagmites
		GameObject stalag = stalagTiles[0];
		
		// Stalagmites
		GameObject upstalag = upStalagTiles[0];
		
		// Slippery rock
		GameObject sliprock = slipRockTiles[0];
		
		// Unstable rock
		GameObject unrock = unstableRockTiles[0];
		
		// Lights
		GameObject light = lightTiles[0];
		
		// Spider
		GameObject spider = spiderTiles[0];
		
		// Unstable wood
		GameObject unwood = unstableWoodTiles[0];
		
		// Metal Beam
		GameObject beam = beamTiles[0];
		
		// Electrical Line
		GameObject elecline = elecLineTiles[0];
		
		// Spring
		GameObject spring = springTiles[0];
		
		// Web
		GameObject web = webTiles[0];
		
		// List of gametiles to be used in the map
		GameObject[] gameTiles = new GameObject[] {
			platform, waterTiles[0], puzzle, platform_left, platform_right, bs, bee, log, bat, stalag, upstalag, sliprock,
			unrock, light, spider, unwood, beam, elecline, spring, web
		};
		
		// Places the tiles based on the settings above
		LayoutObjects(gameTiles, layout);
	}
}