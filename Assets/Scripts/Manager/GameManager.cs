using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null; // Singleton purposes
	public static bool realworld ;	    // Is it the real world dimension or not?
	public static bool doneLoading;	    // Checks if the tilesets are loaded (avoid multiple instances on switching)
	public static bool firstTime;	    // Did the game loads for the first time? (check BoardManager)
	public static bool isPlayable;		// Is the game playable?
	public static Vector3 playerLoc;    // Player's location in the map
	
	public BoardManager boardScript;    // The BoardManager component
	public int numPieces = 0;		    // Number of pieces to be collected (not working atm)
	
	private GameObject player;		    // The player gameobject
	private Player playerInfo;			// Player information (retrieved from above)

	public AudioManager audioManager;
	public bool allPuzzlesCollected = false;

	public bool canLocSwitch = true;

	public ParticleSystem PS_IWorld;
	public GameObject PS_IWorld_instance;
	
	// Timer goes here
	public int seconds = 0;
	public int minutes = 0;

	// Use this for initialization
	void Awake () {
		// Avoid multiple instances of GameManager
		if (instance == null) {
			instance = this;
		}
		else if (instance != this) {
			Destroy(gameObject);
		}
		//instance = null; Commented by Pyl
		// Never destroy this object
		DontDestroyOnLoad(gameObject);
		// Force to show cursor
		Cursor.visible = true;
		// Get BoardManager
		boardScript = GetComponent<BoardManager>();
		// Get Player instance
		player = GameObject.FindGameObjectWithTag("Player");
		playerInfo = player.GetComponent<Player>();
		
		// Resets all static variables...
		realworld = true;	  
		doneLoading = true;	   
		firstTime = true;
		playerLoc = new Vector3(0, 0, 0);
		
		// (Re)Creates the game board
		InitGame();
		
	}

	void Start() {
		//instantiate the ps for the imaginatino world
		//PS_IWorld_instance = Instantiate(PS_IWorld, PS_IWorld.transform.position, PS_IWorld.transform.rotation) as GameObject;
		//Instantiate(PS_IWorld, PS_IWorld.transform.position, PS_IWorld.transform.rotation);
		PS_IWorld_instance = GameObject.Find("PS_IWorld");
		PS_IWorld_instance.GetComponent<ParticleSystem>().enableEmission = false;
		

		Camera.main.transform.rotation = Quaternion.AngleAxis(Random.Range(-1.0f, 1.0f), Vector3.forward);

		if(AudioManager.instance == null) {
			Instantiate(audioManager, transform.position, Quaternion.identity);
		}
		allPuzzlesCollected = false;
		//Cursor.visible = true;
		Time.timeScale = 1; 
		
	}
	
	// Places all the tiles using BoardManager
	public void InitGame () {
		isPlayable = true;
		boardScript.SetupScene(LevelLoader.rwlevel);
		firstTime = false;
		player.transform.position = playerLoc;
	}
	
	// Stops the game manager upon game over
	// TODO: Add more stuffs on game over here ~
	public void GameOver () {
		isPlayable = false;
		//player.GetComponent<Player>().Kill();
		Debug.Log("Calling gameover.");
		Application.LoadLevel("redirect");
		//enabled = false;
		//Application.LoadLevel("gameplay");
		//InstructionsManager.instance.instructions.text = "Imaginarius died. He decided to try again.";
		//Invoke("ResetPlayer", 1f);
	}

	void ResetPlayer() {
		player.GetComponent<Player>().Reset();
	}

	void nextLevel() {
		Application.LoadLevel("next_redirect");
	}

	void FadeOut() {
		Fading.instance.BeginFade(1);		
	}
	
	void RunTimer() {
		Text timer = GameObject.Find ("PuzzleTimer").GetComponent<Text>();
		if (GUIManager.chapterNum % 2 == 0) {
			timer.color = Color.white;
		} else {
			timer.color = Color.gray;
		}
		int time = (int) Time.timeSinceLevelLoad;
		
		seconds = time%60;
		minutes = time/60;
		
		timer.text = string.Format("{0:00}:{1:00}", minutes, seconds);
	}
	
	// Update is called once per frame
	void Update () {
		// Insert timer here
		RunTimer();
		
		if (isPlayable) {
			// DEBUG: Skip level (delete if game is final)
			float skip = Input.GetAxis("SkipLevel");
			if (skip > 0 || skip < 0) {
				if (skip > 0) {
					GUIManager.levelNum += 1;
				} else {
					GUIManager.levelNum -= 1;
				}
				GameOver();
			}
			
			if(playerInfo.puzzlePcs < 3)
				allPuzzlesCollected = false;
			// Goes to the next level when all puzzle pieces are obtained
			if (playerInfo.puzzlePcs == 3) {
				if(!allPuzzlesCollected)
				{
					AudioManager.instance.PlaySFX(AudioManager.SFX.win, 1.0f);
					allPuzzlesCollected = true;
					canLocSwitch = false;
					isPlayable = false;
					Invoke ("nextLevel", 4.5f);
					Invoke("FadeOut", 3f);
				}
				if(Time.timeScale > 0.4f)
					Time.timeScale -= 0.15f * Time.deltaTime;
				
			}
			
			// Goes back to the level list
			if (LevelSelector.levelSelected == 99) {
				Destroy(gameObject);
				LevelSelector.isClicked = false;
				Application.LoadLevel("level_list");
			}
			
			// Loads maps according to current dimension
			if(!doneLoading) {
				
				// Clear the map
				BoardManager.platformLocs.Clear();
				BoardManager.waterLocs.Clear();
				
				// Change map according to alternate dimension
				if (realworld) {
					//Debug.Log("This is the real world.");
					//boardScript.SetupScene(r_test);
					boardScript.SetupScene(LevelLoader.rwlevel);
					PS_IWorld_instance.GetComponent<ParticleSystem>().enableEmission = false;
					//PS_IWorld.Pause();
					//PS_IWorld.GetComponent<Renderer>().enabled = false;
					//DestroyObject(PS_IWorld_instance);
					// Debug.Log("off");
					
				} else {
					//Debug.Log("This is the imaginary world.");
					//boardScript.SetupScene(i_test);
					boardScript.SetupScene(LevelLoader.iwlevel);
					PS_IWorld_instance.GetComponent<ParticleSystem>().enableEmission = true;
					//PS_IWorld.GetComponent<Renderer>().enabled = true;
					//PS_IWorld_instance = Instantiate(PS_IWorld, PS_IWorld.transform.position, PS_IWorld.transform.rotation) as GameObject;
					//PS_IWorld.Play();
					//Instantiate(PS_IWorld, PS_IWorld.transform.position, PS_IWorld.transform.rotation);
					// Debug.Log("on");
				}
				
				// Checks if the player is not inside the platform
				bool canPlay = !WithinBounds(player.GetComponent<Player>().heart.transform, BoardManager.platformLocs);
				
				// If they do, game over. Else, continue the game
				if (!canPlay) {
					GameOver();
				}
				
				// Triggers loading completion
				doneLoading = true;
			}
			
			// Restarts the game by pressing 'R'
			if (Input.GetKey(KeyCode.R)) {
				//Application.LoadLevel("redirect");
				//Application.LoadLevel("gameplay");
				GameOver();
			}
			
			// Moves slowly when player is on water 
			bool inWater = WithinBounds(playerInfo.heart.transform, BoardManager.waterLocs);
			if (inWater && !playerInfo.onGround) {
				playerInfo.inWater = true;
				playerInfo.speed = 1;
				Vector2 force = new Vector2(0, 1) * playerInfo.buoyancy;
				playerInfo.GetComponent<Rigidbody2D>().AddForce(force);
			} else {
				playerInfo.speed = 5;
				playerInfo.inWater = false;
			}
		}
	}
	
	// Checks if an object is within the bounds of the territory 
	public static bool WithinBounds (Transform obj, List<Vector3> territory) {
		int numPlatforms = BoardManager.platformLocs.Count;

		for (int i = 0; i < numPlatforms; i++) {
			Vector3 estimated = new Vector3(Mathf.RoundToInt(obj.transform.position.x), 
				Mathf.RoundToInt(obj.transform.position.y), 0);
			if (territory.Contains(estimated)) {
				return true;
			}
		}
		
		return false;
	}
}
