﻿using UnityEngine;
using System.Collections;

public class EndManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Application.loadedLevelName == "end")
		{
			if(Input.GetKeyDown(KeyCode.C))
			{
				Application.LoadLevel("credits");
			}
		}
		if(Application.loadedLevelName == "credits")
		{
			if(Input.GetKeyDown(KeyCode.Escape))
			{
				Application.LoadLevel("end");
			}
		}

	}
}
