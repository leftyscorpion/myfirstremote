using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InstructionsManager : MonoBehaviour {
	public static InstructionsManager instance;

	public Sprite[] instructionList;
	public Vector3[] locations;
	public SpriteRenderer display;

	public AudioClip clip_e0;
	private AudioSource audioSource;
	
	bool transitionTextFinished = false;

	void Awake() {
		if(instance == null)
			instance = this;
	}
  
	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
		display = GetComponent<SpriteRenderer>();
	}

	void PlayNarration()
	{
		audioSource.Play();
	}

	
	void TransitionTextTo(string str) {
		if(!transitionTextFinished)
		{
			//fadeout text
			if(display.color.a > 0)
			{
				display.color = new Color(1f, 1f, 1f, display.color.a - 0.1f);
			} else {
				transitionTextFinished = true;				
			}
		} else if(transitionTextFinished)
		{
			//change text
			// display.text = "" + str;
			//fadein text
			if(display.color.a < 1)
			{
				display.color = new Color(1f, 1f, 1f, display.color.a + 0.1f);
			}
		}
	}
	
	// Update is called once per frame
	float spaceCount;
	bool getPiece;
	void Update () {
		if (GUIManager.chapterNum == 1) {
			if (GUIManager.levelNum == 1)  {
				if (Player.instance.locswitch) {
					spaceCount++;
					if (spaceCount == 1){
						display.sprite = instructionList[1];
						transform.position = locations[1];
						spaceCount++;
					}
				} else if (Player.instance.puzzlePcs == 1 && !getPiece) {
					display.sprite = instructionList[3];
					transform.position = locations[3];
					getPiece = true;
				} else if (Player.instance.puzzlePcs == 2 && Player.instance.transform.position.y > 2) {
					display.sprite = instructionList[2];
					transform.position = locations[2];
				} else if (Player.instance.puzzlePcs == 3) {
					display.sprite = instructionList[6];
					transform.position = locations[6];
				} else if (getPiece && GameManager.realworld && Player.instance.transform.position.y < 2) { 
					display.sprite = instructionList[4];
					transform.position = locations[4];
				} else if (getPiece && !GameManager.realworld && Player.instance.transform.position.y < 2) {
					display.sprite = instructionList[5];
					transform.position = locations[5];
				}  else  if (GameManager.isPlayable && spaceCount == 0) {
					display.sprite = instructionList[0]; 
					transform.position = locations[0];
				}
			} else if (GUIManager.levelNum == 2) {
				display.sprite = instructionList[7];
				transform.position = locations[7];
			} else {
				display.sprite = null;
			}
		} else if (GUIManager.chapterNum == 2) {
			if (GUIManager.levelNum == 1) {
				display.sprite = instructionList[8];
				transform.position = locations[8];
			} else if (GUIManager.levelNum == 2) {
				display.sprite = instructionList[9];
				transform.position = locations[9];
			} else {
				display.sprite = null;
			}
		} else if (GUIManager.chapterNum == 3) {
			if (GUIManager.levelNum == 1) {
				if (Player.instance.inWater) {
					display.sprite = instructionList[11];
					transform.position = locations[11];
				} else if (GameManager.isPlayable) {
					display.sprite = instructionList[10];
					transform.position = locations[10];
				}
				
			} else if (GUIManager.levelNum == 2) {
				if (GameManager.realworld){
					display.sprite = instructionList[12];
					transform.position = locations[12];
				} else {
					display.sprite = instructionList[13];
					transform.position = locations[13];
				}
			} else {
				display.sprite = null;
			}
		}
	}
}
