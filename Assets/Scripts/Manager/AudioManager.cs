using UnityEngine;
using System;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {
	public static AudioManager instance = null;

	public AudioSource bgm;

	public enum SFX
	{
		footstep_grass1,
		footstep_grass2,
		footstep_snow1,
		footstep_snow2,
		water_drop1,
		water_splash1,
		water_splash2,
		dimension_switch1,
		jump1,
		collectPuzzle,
		win
	}

	// ====================== footstep sfx =======================	
	public AudioClip footstep_grass1;
	public AudioClip footstep_grass2;
	public AudioClip footstep_snow1;
	public AudioClip footstep_snow2;
	// ====================== water sfx =======================
	public AudioClip water_drop1;
	public AudioClip water_splash1;
	public AudioClip water_splash2;
	// ====================== power sfx =======================
	public AudioClip dimesnsion_switch1;
	public AudioClip jump1;
	public AudioSource collectPuzzle;
	public AudioClip win;

	// ====================== surface sfx ==============
	

	void Awake()
	{
		if(instance == null)
			instance = this;

		DontDestroyOnLoad(this.transform.gameObject);
	}

	/// <summary>
	/// Play sound effects!
	/// </summary>
	/// <param name="sfxName">Sfx name.</param>
	/// <param name="vol">Vol.</param>
	public void PlaySFX(SFX sfxName, float vol)
	{

		// ====================== FOOTSTEP sfx =======================
		if(sfxName == SFX.footstep_grass1)
		{
			AudioSource.PlayClipAtPoint(footstep_grass1, this.transform.position, vol);
			
		}
		if(sfxName == SFX.footstep_grass2)
		{
			AudioSource.PlayClipAtPoint(footstep_grass2, this.transform.position, vol);
		}
		if(sfxName == SFX.footstep_snow1)
		{
			AudioSource.PlayClipAtPoint(footstep_snow1, this.transform.position, vol);
		}
		if(sfxName == SFX.footstep_snow2)
		{
			AudioSource.PlayClipAtPoint(footstep_snow2, this.transform.position, vol);
		}

		// ==================== WATER sfx ==========================
		if(sfxName == SFX.water_drop1)
		{
			AudioSource.PlayClipAtPoint(water_drop1, this.transform.position, vol);
		}
		if(sfxName == SFX.water_splash1)
		{
			AudioSource.PlayClipAtPoint(water_splash1, this.transform.position, vol);
		}
		if(sfxName == SFX.water_splash2)
		{
			AudioSource.PlayClipAtPoint(water_splash2, this.transform.position, vol);
		}

		// ==================== POWER sfx ==========================
		if(sfxName == SFX.dimension_switch1)
		{
			AudioSource.PlayClipAtPoint(dimesnsion_switch1, this.transform.position, vol);
		}
		if(sfxName == SFX.jump1)
		{
			AudioSource.PlayClipAtPoint(jump1, this.transform.position, vol);
		}
		if(sfxName == SFX.collectPuzzle)
		{
			if(Player.instance.puzzlePcs == 1)
			{
				collectPuzzle.pitch = 0.8f;
				collectPuzzle.Play();
			} else if(Player.instance.puzzlePcs == 2)
			{
				collectPuzzle.pitch = 0.95f;
				collectPuzzle.Play();
			} else if(Player.instance.puzzlePcs == 3)
			{
				collectPuzzle.pitch = 1.1f;
				collectPuzzle.Play();
			}
		}
		if(sfxName == SFX.win)
		{
			AudioSource.PlayClipAtPoint(win, this.transform.position, vol);
		}
	}


	/// <summary>
	/// Plays background music!
	/// </summary>
	void PlayBGM()
	{

	}

}
