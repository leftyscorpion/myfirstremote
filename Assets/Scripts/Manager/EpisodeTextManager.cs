using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EpisodeTextManager : MonoBehaviour {
	public static EpisodeTextManager instance;

	public Text instructions;
	
	void Awake() {
		if(instance == null)
			instance = this;
	}

	// Use this for initialization
	void Start () {
		int currLvl = LevelSelector.levelSelected;
		instructions.text = "level" + currLvl;
	}
}
