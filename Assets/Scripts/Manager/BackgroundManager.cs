using UnityEngine;
using System.Collections;

public class BackgroundManager : MonoBehaviour
{
    public Sprite[] rwbackground;
    public Sprite[] iwbackground;
    public GameObject gameLight;
    public GameObject playerSpotlight;
    public bool doneSwitching;
    public int currChapter;

    // Use this for initialization
    void Start()
    {
        if (GUIManager.chapterNum == 1)
        { // Change number to what it was remained
            if (GUIManager.levelNum < 4)
            {
                currChapter = 0;
            }
            else if (GUIManager.levelNum < 8)
            {
                currChapter = 1;
            }
            else
            {
                currChapter = 2;
            }
        }
        else
        {
            currChapter = GUIManager.chapterNum - 1;
        }

        GetComponent<SpriteRenderer>().sprite = rwbackground[currChapter];
    }

    void Update()
    {

        if (GUIManager.chapterNum == 4 && !doneSwitching)
        {
            Debug.Log(doneSwitching);
            gameLight.GetComponent<Light>().intensity = 0.15f;
            playerSpotlight.SetActive(true);
            doneSwitching = true;
        }
        else if (GUIManager.chapterNum != 4)
        {
            doneSwitching = false;
            gameLight.GetComponent<Light>().intensity = 1.15f;
            playerSpotlight.SetActive(false);
        }

        if (Input.GetKey(KeyCode.Space) || Player.instance.autoswitch || LevelLoader.doneLoading)
        {
            if (!GameManager.realworld)
            {
                GetComponent<SpriteRenderer>().sprite = iwbackground[currChapter];
            }
            else
            {
                GetComponent<SpriteRenderer>().sprite = rwbackground[currChapter];
            }
        }
    }
}
