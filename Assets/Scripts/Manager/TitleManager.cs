﻿using UnityEngine;
using System.Collections;

public class TitleManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Cursor.visible = false;
		if(Application.loadedLevelName == "title")
		{
			Invoke("FadeOut", 5);			
			
			Invoke("GoToIntro", 10);			
		} else if(Application.loadedLevelName == "demo_introduce")
		{
			Invoke("FadeOut", 3);			
			Invoke("GoToChapter", 7);			
		} else if(Application.loadedLevelName == "chapter_introduce"){
			Invoke("FadeOut", 3);			
			Invoke("GoToGameplay", 7);	
		}
	}

	void FadeOut()
	{
		Fading.instance.BeginFade(1);
	}

	void GoToGameplay()
	{
		Application.LoadLevel("gameplay");
	}

	void GoToIntro()
	{
		Application.LoadLevel("demo_introduce");
	}

	void GoToChapter()
	{
		Application.LoadLevel("chapter_introduce");
	}
	
	// Update is called once per frame
	void Update () {

	}
}
