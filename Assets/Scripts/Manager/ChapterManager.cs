using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChapterManager : MonoBehaviour {
	public static ChapterManager instance = null;

	public string chapter1;
	public string chapter2;
	public string chapter3;
	public string chapter4;

	public Text text_chapter;
	public Text text_title;

	public int currentChapter = 1;

	void Awake() {
		if(instance == null)
			instance = this;

		else if(instance != this)
			Destroy(gameObject);
		/*
		DontDestroyOnLoad(this);
		*/
	}

	// Use this for initialization
	void Start () {
		UpdateText();
		
	}

	void UpdateText()
	{
		switch(currentChapter)
		{
		case 1:
			text_chapter.text = "chapter 1";
			text_title.text = chapter1;
			break;
		case 2:
			text_chapter.text = "chapter 2";
			text_title.text = chapter2;
			break;
		case 3:
			text_chapter.text = "chapter 3";
			text_title.text = chapter3;
			break;
		case 4:
			text_chapter.text = "chapter 4";
			text_title.text = chapter4;
			break;
		}
	}
	/*
	// Update is called once per frame
	void Update () {
		if(LevelSelector.levelNum >= 5)
		{
			currentChapter = 2;
			UpdateText();
		}
	}*/
}
