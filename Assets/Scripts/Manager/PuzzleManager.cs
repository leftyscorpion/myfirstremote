﻿using UnityEngine;
using System.Collections;

public class PuzzleManager : MonoBehaviour {
	
	public GameObject target;
	public Vector3 screenSpace;
	public Vector3 offset;
	
	private bool isClicked;
	
	// Update is called once per frame
	void Update () {
		Debug.Log("Running...");
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit2D hitInfo;
			target = GetClickedObject(out hitInfo);
			if (target != null) {
				Debug.Log("Clicked!");
				isClicked = true;
				screenSpace = Camera.main.WorldToScreenPoint(target.transform.position);
				offset = target.transform.position - Camera.main.ScreenToWorldPoint(
					new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));
			}
		}
		
		if (Input.GetMouseButtonUp(0)) {
			isClicked = false;
		}
		
		if (isClicked) {
			Vector3 currScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
			Vector3 currPosition = Camera.main.ScreenToWorldPoint(currScreenSpace) + offset;
			target.transform.position = currPosition;
		}
	}

	GameObject GetClickedObject (out RaycastHit2D hitInfo) {
		target = null;
		hitInfo = new RaycastHit2D();
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics2D.Raycast(ray.origin, ray.direction)) {
			target = hitInfo.collider.gameObject;
		}
		
		return null;
	}
}
