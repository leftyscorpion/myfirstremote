﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var newPos = Camera.main.transform.position;
		newPos.x = Player.instance.transform.position.x;
		newPos.y = Player.instance.transform.position.y+0.5f;
		Camera.main.transform.position = newPos;
	}
}
