﻿using UnityEngine;
using System.Collections;

public class StickyWeb : MonoBehaviour {
	
	bool playerStuck;
	
	void OnTriggerStay2D(Collider2D other) {
		if (GetComponent<Collider2D>().IsTouching(other)) {
			if (other.tag != "Player" && other.GetComponent<Rigidbody2D>() != null) {
				other.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
			} 
			
			if (playerStuck && !GameManager.realworld) {
				GameManager.isPlayable = false;
				Color temp = GetComponent<SpriteRenderer>().color;
				temp.a = 0.5f;
				Player.instance.GetComponent<SpriteRenderer>().color = temp;
				Destroy (Player.instance.GetComponent<Rigidbody2D>());
				playerStuck = false;
			}
			
			if (other.tag == "Player" && !playerStuck && !GameManager.realworld) {
				StartCoroutine("StuckDelay");
			}
		}
	}
	
	IEnumerator StuckDelay() {
		yield return new WaitForSeconds(1f);
		playerStuck = true;
	}
}
