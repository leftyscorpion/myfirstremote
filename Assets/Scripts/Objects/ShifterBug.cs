﻿using UnityEngine;
using System.Collections;

public class ShifterBug : MonoBehaviour {
	
	private float locationY;
	private float limit = 1.0f;
	private bool alternate = false;
	private bool switchOnce;
	
	// Use this for initialization
	void Start () {
		locationY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		BugMovement();
	}
	
	private void OnTriggerEnter2D(Collider2D other) {
		if (GetComponent<CircleCollider2D>().IsTouching(other)) {
			if (other.tag == "Player") {
				Debug.Log (Player.instance.locswitch);
				if (!switchOnce) {
					StartCoroutine(Cooldown());
				}
			}
		}
	}
	
	IEnumerator Cooldown() {
		Player.instance.autoswitch = true;
		switchOnce = true;
		yield return new WaitForSeconds(5.0f);
		switchOnce = false;
	}
	
	// TODO: Made generalization of this function (can work in any direction)
	private void BugMovement() {
		// Chewck i
		if (transform.position.y > locationY + limit) {
			alternate = true;
		} else if (transform.position.y < locationY - limit) {
			alternate = false;
		} 
		
		if (alternate) {
			transform.Translate(Vector3.down * Time.deltaTime);
		} else {
			transform.Translate(Vector3.up * Time.deltaTime);
		}
	}
}
