﻿using UnityEngine;
using System.Collections;

public class Stalagtite : MonoBehaviour {
	
	public bool isFalling;
		
	// Update is called once per frame
	void FixedUpdate () {
		if (isFalling) {
			StartCoroutine(Falling());
		}
	}
	
	IEnumerator Falling() {
		GetComponent<Animator>().SetBool("isGoingToFall", true);
		yield return new WaitForSeconds(1.0f);
		GetComponent<Rigidbody2D>().isKinematic = false;
		//GetComponent<Animator>().SetBool("isGoingToFall", false);
		gameObject.tag = "Movable";
		isFalling = false;
	}
	
	void OnCollisionEnter2D(Collision2D other) {
		if(other.collider.tag == "Floor") {
			isFalling = false;
			gameObject.tag = "Movable";
			GetComponent<Rigidbody2D>().isKinematic = true;
		} else if (other.collider.tag == "Movable") {
			other.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
		} else if (other.collider.name == "UpStalagtite(Clone)") {
			isFalling = true;
		}
	}
}
