﻿using UnityEngine;
using System.Collections;

public class LightBulb : MonoBehaviour {
	
	public bool isLighted;
	public bool isTouched;
	private GameObject[] lights;
	
	// Use this for initialization
	void Start () {
		lights = GameObject.FindGameObjectsWithTag("LightBulb"); 
	}
	
	// Update is called once per frame
	void Update () {
		if (isLighted) {
			GetComponent<Animator>().SetBool("isTurnedOff", false);
		} else {
			StartCoroutine("DelayOff");
		}
	}
	
	IEnumerator DelayOff() {
		yield return new WaitForSeconds(2.0f);
		GetComponent<Animator>().SetBool("isTurnedOff", true);
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			isLighted = true;
			TurnOffOtherLights();
		}
	}
	
	void TurnOffOtherLights() {
		foreach (GameObject light in lights) {
			if (light != this.gameObject) {	
				Debug.Log ("Did this work?");
				light.GetComponent<LightBulb>().isLighted = false;;
			}
		}
	}
}
