﻿using UnityEngine;
using System.Collections;

public class FloatingLog : MonoBehaviour {

	public float UpwardForce = 3.72f;
	public float offWaterGravity = 0.3f;
	public float onWaterGravity = -0.5f;
	public bool isInWater = false;
	public bool deacGravity;
	public bool isTouched;
	
	void OnCollisionEnter2D(Collision2D other) {
		if (other.collider.tag == "Player") {
			if (GameManager.realworld) {
				deacGravity = true;
				GetComponent<Rigidbody2D>().gravityScale = 1.0f;
			} else {
				deacGravity = false;
			}
		} 
	}
	
	void OnCollisionStay2D(Collision2D other) {
		if (other.collider.tag == "Player" && !GameManager.realworld) {
			isTouched = true;
		}
	}
	
	void OnCollisionExit2D(Collision2D other) {
		if (other.collider.tag == "Player") {
			deacGravity = false;
			isTouched = false;
		}
	}
	
	void OnTriggerStay2D(Collider2D other) {
		if (other.tag == "Water") {
			isInWater = true;
			if (!deacGravity) GetComponent<Rigidbody2D>().gravityScale = onWaterGravity;
		}
	}
	
	void OnTriggerExit2D(Collider2D other){
		isInWater = false;
		if (!deacGravity) GetComponent<Rigidbody2D>().gravityScale = offWaterGravity;
	}
	
	void Update(){
		if (!GameManager.realworld) deacGravity = false;
		
		if (isInWater){
			if (!GameManager.realworld) {
				Vector2 force = Vector2.up * (UpwardForce);
				GetComponent<Rigidbody2D>().AddForce(force);
			} 
		} else {
			Vector2 force = Vector2.down * 10;
			GetComponent<Rigidbody2D>().AddForce(force);
		}
		
		if ((Player.instance.locswitch || Player.instance.autoswitch) && !isTouched) {
			isInWater = false;
		}
	}
}
