﻿using UnityEngine;
using System.Collections;

public class SlippingRock : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		if (!GameManager.realworld) {
			GetComponent<Animator>().SetBool("isImagining", true);
		} else {
			GetComponent<Animator>().SetBool("isImagining", false);
		}
	}
}
