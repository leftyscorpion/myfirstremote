﻿using UnityEngine;
using System.Collections;

public class UnstableObject : MonoBehaviour {
	
	public bool isTouched;
	public bool isStucked;
	public float disappearTime = 1.0f;
	public GameObject[] webs;
			
	void Update () {
		if (isTouched) {
			StartCoroutine("Disintegrate");
		}
	}
	
	// Update is called once per frame
	void OnCollisionStay2D(Collision2D other) {
		if (GetComponent<Collider2D>().IsTouching(other.collider)) {
			if (other.collider.tag == "Player") {
				if (this.name == "UnstableWood(Clone)") {
					SlowDisintegrate();
				}
				
				if (this.name == "UnstableRock(Clone)" && !GameManager.realworld) {
					isTouched = true;
				}
			} 
		}
	}
	
	void SlowDisintegrate() {
		webs = GameObject.FindGameObjectsWithTag("SpiderWeb");
		foreach (GameObject web in webs) {
			bool below = transform.position.y - web.transform.position.y <= 1 &&
				transform.position.y - web.transform.position.y > 0;
			bool align = transform.position.x - web.transform.position.x < 0.01 &&
					transform.position.x - web.transform.position.x > -.01;
			if (below && align) {
				isTouched = false;
				return;
			}
		}
		isTouched = true;
	}
	
	IEnumerator Disintegrate() {
		// Add animation here
		GetComponent<Animator>().SetBool("isDestroyed", true);
		yield return new WaitForSeconds(disappearTime);
		// Destroy object
		Destroy(this.gameObject);
	}
}