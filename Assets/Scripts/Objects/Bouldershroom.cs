﻿using UnityEngine;
using System.Collections;

public class Bouldershroom : MonoBehaviour {
	// Use this for initialization
	public GameObject[] boulshroomTiles;
	public Animator animate;
	public bool onGround;
	public bool hitFloor;
	private Vector3 lastPos;
	
	void Start() {
		animate = GetComponent<Animator>();
	}
	
	void Update() {
		lastPos = transform.position;
		if (Player.instance.locswitch || Player.instance.autoswitch) {
			transform.position = lastPos;
		}
	}
	
	void FixedUpdate () {
		// Deactivate floor if imagining
		if (Input.GetKeyDown(KeyCode.Space)) hitFloor = false;
		
		// Is this shroom floating?
		if (hitFloor) {
			onGround = true;
		} else onGround = false;
	
		// Is he imagining or not?
		if (!GameManager.realworld && GUIManager.chapterNum != 5) {
			//GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
			//if (onGround) GetComponent<Rigidbody2D>().constraints |= RigidbodyConstraints2D.FreezePositionY;
			if (onGround) GetComponent<Rigidbody2D>().isKinematic = true;
			animate.SetBool("isShifting", true);
		} else {
			//GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
			GetComponent<Rigidbody2D>().isKinematic = false;
			animate.SetBool("isShifting", false);
		}
	}
	
	void OnCollisionEnter2D(Collision2D other) {
		if (other.collider.tag == "Floor") {
			Debug.Log ("o.O");
			hitFloor = true;
		} else if (other.collider.tag == "Bouldershroom") {
			BounceAgain();
		}
	}
	
	void OnCollisionExit2D(Collision2D other) {
		if (other.collider.tag == "Player")
			Player.instance.onMovable = false;
		hitFloor = false;
	}
	
	void BounceAgain() {
		// Set velocity according to direction
		GetComponent<Rigidbody2D>().velocity = new Vector2(1, 1);
		
		// Bounce!
		GetComponent<Rigidbody2D>().AddForce(new Vector2 (0 , 10f), ForceMode.Impulse);
	}
}
