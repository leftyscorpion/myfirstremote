﻿using UnityEngine;
using System.Collections;

public class Spider : MonoBehaviour {
	
	public GameObject web;
	private bool isEscaping;
	private bool escapeOnce;
	private bool spitOnce;
	private float distance = 1;
	private int dirX, dirY;
	private Vector3 playerLoc;
	private float lastPosY;
	
	void Awake() {
		lastPosY = transform.position.y;
	}
	
	void Update() {
		if (isEscaping) {
			// Walks away
			transform.Translate(new Vector3(distance*dirX, distance*dirY, 0));
			dirX = 0; dirY = 0;
		} else if (escapeOnce){
			StartCoroutine("GoesDown");
		}
	}
	
	void OnTriggerStay2D(Collider2D other) {
		if (other.tag == "Player") {
			playerLoc = other.transform.position;
			SetDirection(Player.instance.horizontal, Player.instance.vertical);
			StartCoroutine("SpitSomething");
			escapeOnce = true;
		}
	}
	
	void SetDirection(float hor, float ver) {
		Vector3 pos = transform.position;
		if (hor != 0) {
			if (playerLoc.x < pos.x) { // left
				dirX = 1;
			}
			
			else if (playerLoc.x > pos.x) { // right
				dirX = -1;
			}
		}
		
		if (ver != 0) {
			if (playerLoc.y < pos.y) { // down
				dirY = 1;
			}
			
			else if (playerLoc.y > pos.y) { // up
				dirY = -1;
			}
		}
	}
	
	IEnumerator GoesDown() {
		yield return new WaitForSeconds(3.0f);
		if (transform.position.y > lastPosY) transform.Translate(0, -1, 0);
		else if (transform.position.y < lastPosY) transform.Translate(0, -1, 0);
		else escapeOnce = false;
	}
	
	IEnumerator SpitSomething() {
		// If Imaginary world, spit web
		if (!GameManager.realworld && !spitOnce) {
			Instantiate(web, transform.position, Quaternion.identity);
			spitOnce = true;
			// Delay for spitting...
			yield return new WaitForSeconds(0.5f);
			isEscaping = true;
		} else {
			isEscaping = true;
		}
		// Cooldown
		yield return new WaitForSeconds(0.1f);
		isEscaping = false;
		spitOnce = false;
	}
}
