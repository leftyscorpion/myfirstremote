using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PuzzleBehavior : MonoBehaviour {
	public AudioSource sfx_collect;

	void OnTriggerEnter2D(Collider2D colInfo)
	{
		if(colInfo.tag == "Player") {
			// EXPLODE!!!! BOOOM!!!!
			//AudioManager.instance.PlaySFX(AudioManager.SFX.collectPuzzle, 0.6f);
			Instantiate(Player.instance.puzzleBurstPS, transform.position, Player.instance.puzzleBurstPS.transform.rotation);
			Player.instance.puzzlePcs++;
			//Debug.Log("puzzlepcs = " + Player.instance.puzzlePcs);
			HighlightPiece(Player.instance.puzzlePcs);
			// Change value of map matrix
			int x = 8-(int) gameObject.transform.position.y-1;
			int y = (int) gameObject.transform.position.x;
			
			if (GameManager.realworld) {
				if (Player.instance.inWater) {
					LevelLoader.rwlevel[x, y] = '2';
					//GameObject instance = Instantiate(waterTile, new Vector3(other.gameObject.transform.position.x, 
					//        other.gameObject.transform.position.y, 0), Quaternion.identity) as GameObject;
					//instance.transform.SetParent(GameObject.Find("Level").transform);
				}
				else LevelLoader.rwlevel[x, y] = '0';
			} else  {
				if (Player.instance.inWater) {
					LevelLoader.iwlevel[x, y] = '2';
					//GameObject instance = Instantiate(waterTile, new Vector3(other.gameObject.transform.position.x, 
					//         other.gameObject.transform.position.y, 0), Quaternion.identity) as GameObject;
					//instance.transform.SetParent(GameObject.Find("Level").transform);
				}
				else LevelLoader.iwlevel[x, y] = '0';
			}


			//play sfx
			AudioManager.instance.PlaySFX(AudioManager.SFX.collectPuzzle, 1);
			/*
			if(Player.instance.puzzlePcs == 1)
				sfx_collect.pitch = 0.8f;
			else if(Player.instance.puzzlePcs == 2)
				sfx_collect.pitch = 0.9f;
			else if(Player.instance.puzzlePcs == 3)
				sfx_collect.pitch = 1.0f;
			sfx_collect.Play();
			*/

			Invoke("DestroyThis", 5);
			GetComponent<SpriteRenderer>().enabled = false;
			GetComponent<BoxCollider2D>().enabled = false;
		}
	}
	
	void HighlightPiece(int num) {
		switch(num) {
			case 1: GameObject.Find("First").GetComponent<Image>().color = new Color(255f, 255f, 255f, 255f); break;
			case 2: GameObject.Find("Second").GetComponent<Image>().color = new Color(255f, 255f, 255f, 255f); break;
			case 3: GameObject.Find("Third").GetComponent<Image>().color = new Color(255f, 255f, 255f, 255f); break;
		} 
	}
	
	void DestroyThis()
	{
		// Disappear after hit
			gameObject.SetActive(false);
		Destroy (this.gameObject);
	}
}
