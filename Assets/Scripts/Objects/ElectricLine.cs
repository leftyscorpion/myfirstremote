﻿using UnityEngine;
using System.Collections;

public class ElectricLine : MonoBehaviour {
	
	public bool horizontal;
	private Animator animate;
	
	void Start () {
		animate = GetComponent<Animator>();
		if (horizontal) {
			Transform par = transform.parent;
			par.transform.Rotate(0, 0, 90);
			//par.transform.Rotate(new Vector3(0, 0, 0));
		}
		StartCoroutine(RandomStart());
	}
	
	void OnCollisionEnter2D(Collision2D other) {
		if (other.collider.tag == "Player") {
			GameManager.instance.GameOver();
		}
	}
	
	IEnumerator RandomStart() {
		yield return new WaitForSeconds(Random.Range(0.25f, 0.75f));
		StartCoroutine("OnElectricity");
	}
	
	IEnumerator OnElectricity() {
		// Turn of the electric chuchu for 3 seconds
		Debug.Log ("On.");
		animate.SetBool("isTurnedOn", true);
		GetComponent<BoxCollider2D>().enabled = true;
		yield return new WaitForSeconds(1.75f);
		// Turn this off
		StopCoroutine("OnElectricity");
		StartCoroutine("OffElectricity");
	}
	
	IEnumerator OffElectricity() {
		// Turn off again (and disable collision)
		Debug.Log ("Off.");
		animate.SetBool("isTurnedOn", false);
		GetComponent<BoxCollider2D>().enabled = false;
		yield return new WaitForSeconds(1.75f);
		// Turn this off
		StopCoroutine("OffElectricity");
		StartCoroutine("OnElectricity");
	}
}
