﻿using UnityEngine;
using System.Collections;

public class BatTrigger : MonoBehaviour {
	
	private bool fly = false;
	private bool throwStalagtite = false;
	private float distance = 0f;
	private string targetTag = "Stalagtite";
	private Transform target;
	private Animator anim;
	
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	void Update() {
		if (fly && distance < 3f) {
			if (!GameManager.realworld) {
				StartCoroutine(ShakeStalagtite());
			} else {
				// Fly away
				anim.SetBool("isTouching", true);
			}
			fly = false;
		}
		if (anim.GetBool("isTouching")) {
			anim.SetBool("isGoingToFly", true);
			transform.Translate(new Vector3(distance, 0, 0));
			distance += Time.deltaTime/10;
		}
	}
	
	private IEnumerator ShakeStalagtite () {
		anim.SetBool("isImagining", true);
		if (!throwStalagtite) {
			target = GetNearestStalagtite();
			throwStalagtite = true;
		}
		yield return new WaitForSeconds(2.0f);
		if (target != null) {
			target.GetComponent<Stalagtite>().isFalling = true;
		} 
		anim.SetBool("isTouching", true);
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player") {
			// Goes away
			fly = true;
		}
	}
	
	Transform GetNearestStalagtite() {
		float nearestDistanceSqr = Mathf.Infinity;
		GameObject[] taggedGameObjects = GameObject.FindGameObjectsWithTag(targetTag); 
		Transform nearestObj = null;
		// loop through each tagged object, remembering nearest one found
		foreach (GameObject obj in taggedGameObjects) {
			Vector3 objectPos = obj.transform.position;
			float distanceSqr = (objectPos - transform.position).sqrMagnitude;
			if (distanceSqr < nearestDistanceSqr) {
				nearestObj = obj.transform;
				nearestDistanceSqr = distanceSqr;
			}
		}
		return nearestObj;
	}
}