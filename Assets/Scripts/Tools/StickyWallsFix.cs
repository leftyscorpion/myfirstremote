﻿using UnityEngine;
using System.Collections;

public class StickyWallsFix : MonoBehaviour {
	
	
	void OnCollisionEnter2D(Collision2D colInfo)
	{
		if(colInfo.transform.tag == "Player")
		{
			Vector2 colliderSize = GetComponent<BoxCollider2D>().size;
			colliderSize.x = 1.0f;
			GetComponent<BoxCollider2D>().size = colliderSize;
		}
	}
	
	void OnCollisionExit2D(Collision2D colInfo)
	{
		if(colInfo.transform.tag == "Player")
		{
			//Debug.Log("detected!");
			Vector2 colliderSize = GetComponent<BoxCollider2D>().size;
			colliderSize.x = 0.9f;
			GetComponent<BoxCollider2D>().size = colliderSize;
		}
	}
}
