﻿using UnityEngine;
using System.Collections;

public class LevelInfo : MonoBehaviour {
		
	// Level number of this button
	public int lvlNum;
	
	// Current status of this level
	// locked = -1 | ready = 0 | complete = 1
	public int status;
}
