﻿using UnityEngine;
using System.Collections;
using System.IO;

public class LevelLoader : MonoBehaviour {
	public static LevelLoader instance = null;

	public static char[,] rwlevel = new char[8, 12];
	public static char[,] iwlevel = new char[8, 12];
	
	// Files...
	protected FileInfo source = null;		// Textfile path
	protected StreamReader reader = null;	// Reads the textfile
	protected string currLine = " "; 		// String holder for reader
	
	// TEST: Using TextAssets instead of StreamReader
	TextAsset src; 
	
	// For index-location for map matrices (which part of the scene has been scanned here?)
	private int rwline = 0;
	private int iwline = 0;
	public static bool doneLoading;
	private bool isRealWorld = true; // default
	
	// Use this for initialization
	void Awake () {
		if(instance == null)
			instance = this;

		try {
			doneLoading = false;
			// If you want to load the game from the first level, uncomment the code below.
			src = Resources.Load("Levels/Chapter/" + LevelSelector.chapterSelected + "/level_" + LevelSelector.levelSelected) as TextAsset;
			
			// If you just want to test the game, change 'x' to the level number
			// Still note the procedures on making the levels (see Resources > Levels > _README.txt for more info
			//src = Resources.Load("Levels/level_5") as TextAsset;
			
			ReadTextFile();
		} catch (FileNotFoundException ex) {
			Debug.Log("Missing files, I think?");
			Debug.Log(ex);
		}
	}
	
	public void ReadTextFile() {
		string[] lines = src.text.Split('\n'); 
		string currLine = "";
		for (int q = 0; q < lines.Length; q++) {
			currLine = lines[q];
			
			if(!(currLine.StartsWith("#"))) {
				if (currLine.StartsWith("End")) {
					//reader.Close();
					doneLoading = true;
				}
				else if (currLine.StartsWith("Real")) {
					isRealWorld = true;
				}
				else if (currLine.StartsWith("Imag")) {
					isRealWorld = false;
				}
				else {
					string[] currSet = ExtractString(currLine);
					char[] convertedSet = new char[12];
					
					for(int i = 0; i < currSet.Length; i++) {
						convertedSet[i] = char.Parse(currSet[i]);
					}
					
					if (isRealWorld) {
						for(int i = 0; i < 12 && rwline < 8; i++) {
							rwlevel[rwline, i] = convertedSet[i];
						}
						rwline++;
					} else {
						for(int i = 0; i < 12 && iwline < 8; i++) {
							iwlevel[iwline, i] = convertedSet[i];
						}
						iwline++;
					}
					
					currSet = null;
					convertedSet = null;
				}
			}
		}
	}
	
	// Reads the textfile and converts the result to map matrix
	public void ReadFile () {
		while (!doneLoading) {
			currLine = reader.ReadLine();
			
			if(!(currLine.StartsWith("#"))) {
				if (currLine.StartsWith("End")) {
					reader.Close();
					doneLoading = true;
				}
				else if (currLine.StartsWith("Real")) {
					isRealWorld = true;
				}
				else if (currLine.StartsWith("Imag")) {
					isRealWorld = false;
				}
				else {
					string[] currSet = ExtractString(currLine);
					char[] convertedSet = new char[12];
					
					for(int i = 0; i < currSet.Length; i++) {
						convertedSet[i] = char.Parse(currSet[i]);
					}
					
					if (isRealWorld) {
						for(int i = 0; i < 12 && rwline < 8; i++) {
							rwlevel[rwline, i] = convertedSet[i];
						}
						rwline++;
					} else {
						for(int i = 0; i < 12 && iwline < 8; i++) {
							iwlevel[iwline, i] = convertedSet[i];
						}
						iwline++;
					}
					
					currSet = null;
					convertedSet = null;
				}
			}
		}
	}
	
	private string[] ExtractString (string extract) {
		return extract.Split(' ');
	}
}
