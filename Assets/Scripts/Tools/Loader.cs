using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour {
	
	public GameObject gameManager;
	public GameObject levelManager;
	
	private bool isInit = false;
	
	void Awake () {
		Instantiate(levelManager);
	}
	
	void Update () {
		if (LevelLoader.doneLoading && !isInit) {
			// Checks if GameManager was already initiated	
			if (GameManager.instance == null) {
				Instantiate(gameManager);
			}
			isInit = true;
		}
	}
}
