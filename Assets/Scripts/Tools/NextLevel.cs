using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour {
	
	void Start () {
		if(GUIManager.levelNum < 10) {
			Destroy (GameObject.FindGameObjectWithTag("Manager"));
			GUIManager.levelNum += 1;
			LevelLoader.rwlevel = new char[8, 12];
			LevelLoader.iwlevel = new char[8, 12];
			BoardManager.ClearLocations();
			GameManager.instance.allPuzzlesCollected = true;
			Application.LoadLevel("gameplay");
		} else {
			Destroy (GameObject.FindGameObjectWithTag("Manager"));
			if (GUIManager.tempNum[GUIManager.chapterNum-1] < GUIManager.levelNum)
				GUIManager.tempNum[GUIManager.chapterNum-1] = GUIManager.levelNum;
			Application.LoadLevel("scene_list");
		}

	}
}
