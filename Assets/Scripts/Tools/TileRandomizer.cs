﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileRandomizer : MonoBehaviour {
	public List<Sprite> spriteList_normal;
	public List<Sprite> spriteList_platform;
	public List<Sprite> spriteList_corner;
	

	// Use this for initialization
	void Start () {
		//GetComponent<SpriteRenderer>().sprite = spriteList_normal[GUIManager.chapterNum-1];
	}
	
	public void TileAsPlatform() {
		//GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);
		GetComponent<SpriteRenderer>().sprite = spriteList_platform[GUIManager.chapterNum-1];
	}

	public void TileAsCorner() {
		//GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);
		GetComponent<SpriteRenderer>().sprite = spriteList_corner[GUIManager.chapterNum-1];		
	}
	
	public void TileAsTop() {
		GetComponent<SpriteRenderer>().sprite = spriteList_normal[GUIManager.chapterNum-1];
	}
}
