﻿using UnityEngine;
using System.Collections;

public class Redirect : MonoBehaviour {

	void Awake () {
		Destroy (GameObject.FindGameObjectWithTag("Manager"));
		GameManager.doneLoading = false;
		if (GUIManager.quitGame) {
			GUIManager.quitGame = false;
			if (GUIManager.tempNum[GUIManager.chapterNum-1] < GUIManager.levelNum)
				GUIManager.tempNum[GUIManager.chapterNum-1] = GUIManager.levelNum;
			Application.LoadLevel("scene_list");
		} else if (GUIManager.mainMenu) {
			GUIManager.mainMenu = false;
			GUIManager.tempNum[GUIManager.chapterNum-1] = GUIManager.levelNum;
			Application.LoadLevel("main_menu");
		}
		else Invoke("LoadLevel", 0.1f);
	}
	
	void LoadLevel() {
		BoardManager.ClearLocations();
		Application.LoadLevel("gameplay");
	}
}
