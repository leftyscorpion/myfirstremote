﻿using UnityEngine;
using System.Collections;

public class LevelSelector : MonoBehaviour {
	public static LevelSelector instance = null;
	
	public static bool isClicked = false;
	public static int chapterSelected;
	public static int levelSelected;

	public int levelNum;
	//public bool levelDebugEnabled;

	void Awake() {
		
		chapterSelected = GUIManager.chapterNum;
		levelSelected = GUIManager.levelNum;
		
	}
	
	void OnMouseDown() {
		//isClicked = true;
		//clickedNum = levelNum;
	}
}
