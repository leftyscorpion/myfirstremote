﻿using UnityEngine;
using System.Collections;

public class PSDestroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//this.GetComponent<Renderer>().sortingLayerName = "Default";
		//this.GetComponent<Renderer>().sortingOrder = 4;
		GetComponent<Renderer>().sortingLayerName = "Figures";
		GetComponent<Renderer>().sortingOrder = 5;
	}
	
	// Update is called once per frame
	void Update () {
		Invoke ("DestroyThis", GetComponent<ParticleSystem>().startLifetime);
	}

	void DestroyThis()
	{
		Destroy(this.gameObject);
	}
}
