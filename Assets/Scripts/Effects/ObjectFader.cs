﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObjectFader : MonoBehaviour {
	public Text text;
	public float fadeSpeed;

	// Use this for initialization
	void Start () {
		text.color = new Color(text.color.r, text.color.g, text.color.b, 0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(text.color.a < 1)
		{
			text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a + fadeSpeed);
		}
	}
}
