﻿using UnityEngine;
using System.Collections;

public class CameraEffects : MonoBehaviour {
	public static CameraEffects instance = null;
	[HideInInspector] public UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration aberration;
	[HideInInspector] public UnityStandardAssets.ImageEffects.ColorCorrectionCurves colorCorrection;
	[HideInInspector] public UnityStandardAssets.ImageEffects.SunShafts sun;
	

	public float aberration_max;
	public float aberration_min;
	public float aberration_decrementer;

	public float intensity_min;
	public float intensity_max;

	public float blur_max;
	public float blur_min;
	public float blur_decrementer;

	public float saturation_max;
	public float saturation_min;

	public float sun_maxIntensity;
	public float sun_minIntensity;

	void Awake()
	{
		if(instance == null)
			instance = this;
	}

	// Use this for initialization
	void Start () {
		aberration = GetComponent<UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration>();
		colorCorrection = GetComponent<UnityStandardAssets.ImageEffects.ColorCorrectionCurves>();
		sun = GetComponent<UnityStandardAssets.ImageEffects.SunShafts>();
		sun.sunShaftIntensity = sun_maxIntensity;
	}
	
	// Update is called once per frame
	void Update () {
		if(aberration.chromaticAberration > aberration_min)
		{
			aberration.chromaticAberration -= aberration_decrementer;
		}

		if(aberration.blur > blur_min)
		{
			aberration.blur -= blur_decrementer;
		}

		UpdateSunBehavior();
	}

	private void UpdateSunBehavior()
	{
		if(sun.sunShaftIntensity > sun_minIntensity)
		{
			sun.sunShaftIntensity -= 0.01f;
		}
	}

	public void TriggerAberration()
	{
		aberration.chromaticAberration = aberration_max;
		aberration.blur = blur_max;
	}
}
