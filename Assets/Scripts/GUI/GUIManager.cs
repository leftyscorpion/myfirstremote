using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIManager : MonoBehaviour {
	public static GUIManager instance = null; // Singleton purposes
	public static int chapterNum = 1;
	public static int levelNum = 1;
	public static bool quitGame;
	public static bool mainMenu;
	public static int[] tempNum = {1, 1, 1, 1, 1};
	public static bool changeOnce;
	
	void Awake() {
		// Avoid multiple instances of GameManager
		if (instance == null) {
			instance = this;
		}
		else if (instance != this) {
			Destroy(gameObject);
		}
		//instance = null; Commented by Pyl
		// Never destroy this object
		DontDestroyOnLoad(gameObject);
	}
	
	void Update() {
		if (!changeOnce) {
			ChangeMusicButton();
			changeOnce = true;
		}
	}
	
	public void RestartGame() {
		Application.LoadLevel("redirect");
	}
	
	public void PauseGame() {
		Time.timeScale = 0.0f;
		GameObject.Find ("PauseMenu").GetComponent<Animator>().SetBool("openMenu", true);
	}
	
	public void ResumeGame() {
		Time.timeScale = 1.0f;
		GameObject.Find ("PauseMenu").GetComponent<Animator>().SetBool("openMenu", false); 
	}
	
	public void MuteButton() {
		AudioListener.pause = !AudioListener.pause;
	}
	
	public void ChangeMusicButton() {
		if (AudioListener.pause) {
			GameObject.Find("AudioButton").GetComponent<Image>().sprite = Resources.Load<Sprite>("GUI/btn_stopmusic");
		} else {
			GameObject.Find("AudioButton").GetComponent<Image>().sprite = Resources.Load<Sprite>("GUI/btn_playmusic");
		}
		changeOnce = false;
	}
	
	public void ChapterSelect(int chap) {
		Debug.Log (Time.timeScale);
		Time.timeScale = 1; 
		chapterNum = chap;
		ChangeColor();
		GameObject.Find("Main Camera").GetComponent<PopulateLevelList>().isClicked = true;
		GameObject.Find ("LevelSelect").GetComponent<Animator>().SetBool("openMenu", true);
	}
	
	public void CloseLevelSelect() {
		GameObject.Find ("LevelSelect").GetComponent<Animator>().SetBool("openMenu", false);
	}
	
	public static void LevelSelect(int lvl) {
		levelNum = lvl;
		Application.LoadLevel("redirect");
	}
	
	public void BackToLevelSelect() {
		Time.timeScale = 1.0f;
		quitGame = true;
		Application.LoadLevel("redirect");
	}
	
	public void BackToMainMenu() {
		Time.timeScale = 1.0f;
		mainMenu = true;
		Application.LoadLevel("redirect");
	}
	
	public void StartGame() {
		Application.LoadLevel("scene_list");
	}
	
	public void EndSession() {
		Application.LoadLevel("research_end");
	}
	
	public void OpenForm() {
		Application.OpenURL("http://goo.gl/forms/3BRWCwhj5O");
	}
	
	public void OpenInstruction() {
		GameObject.Find ("InstructionHolder").GetComponent<Animator>().SetBool("openInstruction", true);
	}
	
	// Update is called once per frame
	public void ChangeColor () {
		Image levelbox = GameObject.Find("LevelSelect").GetComponent<Image>();
		switch(GUIManager.chapterNum) {
			case 1: ApplyAlpha(levelbox, Color.green); break;
			case 2: ApplyAlpha(levelbox, Color.magenta); break;
			case 3: ApplyAlpha(levelbox, Color.cyan); break;
			case 4: ApplyAlpha(levelbox, Color.blue); break;
			case 5: ApplyAlpha(levelbox, Color.yellow); break;
			default: break;
		}
	}
	
	public void ApplyAlpha(Image image, Color c) {
		image.color = c;
		Color temp = image.color;
		temp.a = 0.25f;
		image.color = temp;
	}
}
