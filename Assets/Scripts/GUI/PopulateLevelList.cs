﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class PopulateLevelList : MonoBehaviour {
	
	public int currNum = 1;
	public bool isClicked;
	public GameObject btn;
	public GameObject container;
	public Sprite disabled;
	private Transform lvlparent;
	private SpriteState st;
	
	
	// Use this for initialization
	void Start () {
		lvlparent = container.transform;
		btn = GameObject.Find ("DummyBtn");
	}
	
	void Update() {
		if (isClicked) {
			if (GameObject.Find ("LevelSelect").GetComponent<Animator>().GetBool("openMenu")) {
				PopulateList();
				isClicked = false;
			} 
		}
	}
	
	public void PopulateList() {
		// Height + width of the levels
		int row = 2;
		int col = 5;
		
		// Reset counter
		currNum = 1;
		
		// TEST: Add restrictions/locked areas
		int level = GUIManager.tempNum[GUIManager.chapterNum-1];
		
		/// Overriding sprite chuchu
		st = new SpriteState();
		st.disabledSprite = disabled;
		st.highlightedSprite = disabled;
		st.pressedSprite = disabled;
		
		// Deploy all the buttons
		for (int i = row; i > 0; i--) {
			for (int j = 0; j < col; j++) {
				if (currNum > level) {
					GameObject lvl = DeployButton(i, j);
					lvl.GetComponent<LevelInfo>().status = -1;
					lvl.GetComponent<Image>().sprite = disabled;
					lvl.GetComponent<Button>().spriteState = st;
				} else if (currNum == level) {
					GameObject lvl = DeployButton(i, j);
					lvl.GetComponent<LevelInfo>().status = 0;
					OpenLevel(lvl);
				} else if (currNum < level) {
					GameObject lvl = DeployButton(i, j);
					lvl.GetComponent<LevelInfo>().status = 1;
					OpenLevel(lvl);
				}
				// Increment by 1
				currNum++;
			}
		}
	}
	
	GameObject DeployButton(int x, int y) {
		// Distance between buttons
		int distx = 110;
		int disty = 90;
		
		// Deploy buttons (reversing y and x for linear arrangement)
		GameObject instance = Instantiate(btn, new Vector3((distx*y), (disty*x), 0), Quaternion.identity) as GameObject;
		instance.transform.SetParent(lvlparent, false);
		
		// Change text
		string cn_string = "" + currNum;
		instance.transform.FindChild("Num").GetComponent<Text>().text = cn_string;
		
		// Return instance (for modification)
		return instance;
	}
	
	void OpenLevel(GameObject lvlbtn) {
		// Add button events
		lvlbtn.GetComponent<LevelInfo>().lvlNum = currNum;
		lvlbtn.GetComponent<Button>().onClick.AddListener(() => GUIManager.LevelSelect(lvlbtn.GetComponent<LevelInfo>().lvlNum));	
	}
}
