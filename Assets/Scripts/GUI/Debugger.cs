﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Debugger : MonoBehaviour {
	
	// Declare these in your class (forgot the source... basta this is for frame count)
	int m_frameCounter = 0;
	float m_timeCounter = 0.0f;
	float m_lastFramerate = 0.0f;
	public float m_refreshTime = 0.5f;
	
	// Temporary variables for debugging
	
	// Use this for initialization
	void Start () {
		DebugGameInfo();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.isPlayable) {
			DebugManagers();
			GetFrameRate();
		}
	}
	
	void DebugManagers() {
		GameObject.Find("LM").GetComponent<Text>().text = ("Chapter1:" + (GUIManager.tempNum[0]));
		GameObject.Find("BM").GetComponent<Text>().text = ("Chapter2:" + (GUIManager.tempNum[1]));
		GameObject.Find("GM").GetComponent<Text>().text = ("Chapter3:" + (GUIManager.tempNum[2]));
	}
	
	void DebugGameInfo() {
		GameObject.Find("OS").GetComponent<Text>().text = ("OS:" + SystemInfo.operatingSystem);
		GameObject.Find("Graphics").GetComponent<Text>().text = ("GRAPHICS:" + SystemInfo.graphicsDeviceVendor + " " + SystemInfo.graphicsDeviceVersion);
		GameObject.Find("Memory").GetComponent<Text>().text = ("VIDEOMEMSIZE:" + SystemInfo.graphicsMemorySize);
	}
	
	void GetFrameRate() {
		if( m_timeCounter < m_refreshTime ) {
			m_timeCounter += Time.deltaTime;
			m_frameCounter++;
		} else {
			//This code will break if you set your m_refreshTime to 0, which makes no sense.
			m_lastFramerate = (float)m_frameCounter/m_timeCounter;
			m_frameCounter = 0;
			m_timeCounter = 0.0f;
		}
		GameObject.Find("FrameRate").GetComponent<Text>().text = ("FRAMERATE:" + m_lastFramerate);
	}
}
