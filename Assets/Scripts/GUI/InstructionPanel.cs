﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InstructionPanel : MonoBehaviour
{

    public Sprite[] instruc;
    public int currPage = 1;
    public int temp;
    private Image page;
    public int counter;
    public bool autoPopUpCount;
    public bool[] instrucStatus;

    // Use this for initialization
    void Awake()
    {
        page = GetComponent<Image>();
        instrucStatus = new bool[instruc.Length];
        currPage = 1;
        for (int i = 0; i < instrucStatus.Length; i++)
        {
            instrucStatus[i] = false;
        }
    }

    void Update()
    {
        PopUpInstruction();
    }

    public void SwitchInstruction(int turn)
    {
        Debug.Log(currPage);
        if (currPage < 1) currPage = 1;
        else if (currPage > instruc.Length) currPage = 22;
        else
        {
            temp += turn;
            currPage = temp;
        }
        page.sprite = instruc[currPage - 1];
    }

    public void CloseInstruction()
    {
        GameObject.Find("InstructionHolder").GetComponent<Animator>().SetBool("openInstruction", false);
        Time.timeScale = 1.0f;
    }

    public void PopUpInstruction()
    {
        int cn = GUIManager.chapterNum;
        int ln = GUIManager.levelNum;
        bool[] st = instrucStatus;
        if (cn == 1)
        {
            if (ln == 1 && st[0] == false)
            {
                AutoPopUp(1);
                st[0] = true;
            }
            else if (ln == 2 && st[1] == false)
            {
                AutoPopUp(2);
                st[1] = true;
            }
            else if (ln == 3 && st[2] == false)
            {
                AutoPopUp(5);
                st[2] = true;
            }
            else if (ln == 4 && st[3] == false)
            {
                AutoPopUp(6);
                st[3] = true;
            }
            else if (ln == 6 && st[4] == false)
            {
                AutoPopUp(7);
                st[4] = true;
            }
            else if (ln == 8 && st[5] == false)
            {
                AutoPopUp(8);
                st[5] = true;
            }
            else if (ln == 10 && st[6] == false)
            {
                AutoPopUp(9);
                st[6] = true;
            }

        }
        else if (cn == 4)
        {
            if (ln == 3 && st[7] == false)
            {
                AutoPopUp(11);
                st[7] = true;
            }
            else if (ln == 6 && st[8] == false)
            {
                AutoPopUp(12);
                st[8] = true;
            }
            else if (ln == 7 && st[9] == false)
            {
                AutoPopUp(13);
                st[9] = true;
            }
            else if (ln == 1 && st[10] == false)
            {
                AutoPopUp(10);
                st[10] = true;
            }
        }
        else if (cn == 5)
        {
            if (ln == 1 && st[11] == false)
            {
                AutoPopUp(16);
                st[11] = true;
            }
            else if (ln == 4 && st[12] == false)
            {
                AutoPopUp(17);
                st[12] = true;
            }
            else if (ln == 5 && st[13] == false)
            {
                AutoPopUp(14);
                st[13] = true;
            }
        }
    }

    public void AutoPopUp(int p)
    {
        GameObject.Find("InstructionHolder").GetComponent<Animator>().SetBool("openInstruction", true);
        StartCoroutine("PauseWhileDisplaying");
        page.sprite = instruc[p - 1];
        currPage = p;
    }


    IEnumerator PauseWhileDisplaying()
    {
        yield return new WaitForSeconds(1.0f);
        Time.timeScale = 0.0f;
    }
}
