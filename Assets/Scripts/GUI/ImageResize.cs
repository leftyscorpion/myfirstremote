﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageResize : MonoBehaviour {
	
	public Sprite[] dimensionStatus;
	private Image notif;
	
	// Use this for initialization
	void Start () {
		notif = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.isPlayable) {
			if (GameManager.realworld) {
				notif.sprite = dimensionStatus[0];
			} else {
				notif.sprite = dimensionStatus[1];
			}
		}
	}
}
